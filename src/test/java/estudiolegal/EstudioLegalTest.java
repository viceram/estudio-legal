package estudiolegal;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import estudiolegal.excepcion.*;
import estudiolegal.domicilio.*;

public class EstudioLegalTest {
    
    EstudioLegal estudio;
    DomicilioSimple domicilio;
    Cliente cliente;

    @Before
    public void before(){
        estudio = new EstudioLegal("4-334-5", "4543298");

        domicilio = new DomicilioSimple("467", "San Martin");
        cliente = new Cliente("Lopez", "Juan","Asunto", "lopez928@gmail,com","4566654", 30224440);
        cliente.setDomicilio(domicilio);
    }

    @Test 
    public void testAgregarCliente() throws ClienteExistenteException, DomicilioNoAgregadoException {
        

        estudio.agregarClientes(cliente);
        assertEquals(1, estudio.getClientes().size());
    }

    @Test
    public void testBuscarCliente() throws ClienteExistenteException, ClienteInexistenteException,
            DomicilioNoAgregadoException {

        estudio.agregarClientes(cliente);
        Cliente clienteActual = estudio.getCliente(30224440);
        assertEquals(cliente, clienteActual);
    }

    @Test
    public void testModificarCliente() throws ClienteExistenteException, ClienteInexistenteException,
            DomicilioNoAgregadoException {

        estudio.agregarClientes(cliente);
        DomicilioBarrial domicilioCambiado = new DomicilioBarrial("25", "27 viviendas", 2);
        Cliente clienteModificado = new 
        Cliente("Lopez", "Juan","Asunto", "lopez928@gmail,com","4566654", 30224440);

        estudio.modificarCliente(clienteModificado);
        Cliente clienteActual = estudio.getCliente(30224440);
        assertEquals(clienteModificado, clienteActual);
    }

    @Test 
    public void testEliminarCliente() throws ClienteExistenteException, ClienteInexistenteException,
            DomicilioNoAgregadoException {

        estudio.agregarClientes(cliente);
        estudio.eliminarCliente(30224440);
        assertEquals(0, estudio.getClientes().size());
    }

    @Test (expected = ClienteExistenteException.class)
    public void testAgregarClienteExistente() throws ClienteExistenteException, DomicilioNoAgregadoException {
        estudio.agregarClientes(cliente);
        estudio.agregarClientes(cliente);
    }

    @Test (expected = ClienteInexistenteException.class)
    public void buscarClienteInexistente() throws ClienteExistenteException,
                        ClienteInexistenteException, DomicilioNoAgregadoException {

        estudio.agregarClientes(cliente);

        estudio.getCliente(23);
    }

    @Test
    public void ordenarPorDni() throws ClienteExistenteException, DomicilioNoAgregadoException {

        estudio.agregarClientes(cliente);
        DomicilioBarrial domicilio2 = new DomicilioBarrial("25", "27 viviendas", 2);
        Cliente cliente2 = new 
        Cliente("Carrizo", "Ivan","Asunto", "carrizoI@gmail,com","4534747", 44566641);
        cliente.setDomicilio(domicilio2);
        estudio.agregarClientes(cliente2);

        estudio.ordenarClientesPorDni();

        ArrayList<Cliente> listaEsperada = new ArrayList<Cliente>();
        listaEsperada.add(cliente);
        listaEsperada.add(cliente2);

        assertArrayEquals(listaEsperada.toArray(), estudio.getClientes().toArray());
    }

    @Test
    public void ordenarPorApellido() throws ClienteExistenteException, DomicilioNoAgregadoException {

        estudio.agregarClientes(cliente);
        DomicilioBarrial domicilio2 = new DomicilioBarrial("25", "27 viviendas", 2);
        Cliente cliente2 = new Cliente("Carrizo", "Ivan","Asunto", "carrizoI@gmail,com","4534747", 44566641);
        cliente.setDomicilio(domicilio2);

        estudio.agregarClientes(cliente2);

        estudio.ordenarClientesPorApellido();

        ArrayList<Cliente> listaEsperada = new ArrayList<Cliente>();
        listaEsperada.add(cliente2);
        listaEsperada.add(cliente);

        assertArrayEquals(listaEsperada.toArray(), estudio.getClientes().toArray());
    }
}
