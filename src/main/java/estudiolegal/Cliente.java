package estudiolegal;

import estudiolegal.domicilio.*;

public class Cliente implements Comparable{
    private String apellido;
    private String nombre;
    private String asunto;
    private String correoElectronico;
    private String telefono;
    private Domicilio domicilio;
    private Integer dni;

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public Cliente(String apellido, String nombre,String asunto, String correoElectronico, String telefono,
                 Integer dni){
        this.apellido = apellido;
        this.nombre = nombre;
        this.asunto = asunto;
        this.correoElectronico=correoElectronico;
        this.telefono=telefono;
        this.dni=dni;
    }

    //setter
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    public void setDomicilio(Domicilio domicilio){
        this.domicilio = domicilio;
    }
    
    //getter
    public String getApellido(){
        return apellido;
    }

    public String getNombre(){
        return nombre;
    }

    public String getAsunto(){
        return asunto;
    }

    public String getCorreoElectronico(){
        return correoElectronico;
    }

    public String getTelefono(){
        return telefono;
    }

    public Integer getDni(){
        return dni;
    }

    public Domicilio getDomicilio(){
        return domicilio;
    }

    @Override
    public int compareTo(Object objeto){
        Cliente cliente = (Cliente) objeto;
        return this.dni.compareTo(cliente.getDni());
    }
    
    public String getNombreCompleto(){
        return getApellido() + " " + getNombre();
    }

    public String mostrarDatosDelCliente(){
        return "Nombre completo: " + getApellido() + " " + getNombre() + "- DNI: " + getDni() + "\n" + 
        " Asusto de visita " + getAsunto() + "\n"
        + "- Domicilio: " + getDomicilio().mostrarDatosDelDomicilio()
        + "Informacion de contacto: Telefono " + getTelefono() + " Email " + getCorreoElectronico();
    }
}
