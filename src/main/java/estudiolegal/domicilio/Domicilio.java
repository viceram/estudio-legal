package estudiolegal.domicilio;

public abstract class Domicilio {
    
    private String numeroDeDomicilio;

    public Domicilio( String numeroDeDomicilio){
        this.numeroDeDomicilio=numeroDeDomicilio;
    }

    public void setNumeroDomicilio(String numeroDeDomicilio){
        this.numeroDeDomicilio=numeroDeDomicilio;
    }

    public String getNumeroDomcilio(){
        return numeroDeDomicilio;
    }

    public abstract String mostrarDatosDelDomicilio();

}
