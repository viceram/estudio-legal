package estudiolegal.domicilio;

public class DomicilioSimple extends Domicilio {

    private String calle;

    public DomicilioSimple(String numeroDeDomicilio, String calle) {
        super(numeroDeDomicilio);
        this.calle=calle;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    @Override
    public String mostrarDatosDelDomicilio() {
        return "Calle: " + getCalle() + " Domcilio numero:" + getNumeroDomcilio();
    }
}
