package estudiolegal.domicilio;

public class DomicilioEdificio extends Domicilio {

    private String piso;
    private String departamento;
    private String calle;

    public DomicilioEdificio(String numeroDeDomicilio, String piso, String departamento, String calle){
        super(numeroDeDomicilio);
        this.piso=piso;
        this.departamento=departamento;
        this.calle=calle;
    }

    //---------------getter----------------
    public String getDepartamento() {
        return departamento;
    }

    public String getPiso() {
        return piso;
    }

    public String getCalle() {
        return calle;
    }

    //-----------------setter-------------------
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    @Override
    public String mostrarDatosDelDomicilio(){
        return "Calle: " + getCalle() + ", Piso: " + getPiso() + " Departamento numero: " + getDepartamento();
    }
}
