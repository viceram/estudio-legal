package estudiolegal.domicilio;

public class DomicilioBarrial extends Domicilio {

    private String nombreDelBarrio;
    private Integer manzana;


    public DomicilioBarrial(String numeroDeDomicilio, String nombreDelBarrio, Integer manzana){
        super(numeroDeDomicilio);
        this.nombreDelBarrio=nombreDelBarrio;
        this.manzana=manzana;
    }

    //-------------------getter----------------------
    public Integer getManzana(){
        return manzana;
    }

    public String getNombreDelBarrio() {
        return nombreDelBarrio;
    }

    //------------------setter-----------------------
    public void setManzana(Integer manzana) {
        this.manzana = manzana;
    }

    public void setNombreDelBarrio(String nombreDelBarrio) {
        this.nombreDelBarrio = nombreDelBarrio;
    }

    @Override
    public String mostrarDatosDelDomicilio() {
        return "Nombre del barrio: " + getNombreDelBarrio() + ", Manzana numero: " + getManzana()
        + ", Casa numero: " + getNumeroDomcilio();
    }
    
}
