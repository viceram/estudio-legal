package estudiolegal.excepcion;

public class DomicilioNoAgregadoException extends Exception{
    public DomicilioNoAgregadoException(){
        super("Se requiere que agregue domicilio");
    }
}
