package estudiolegal.excepcion;

public class ClienteExistenteException extends Exception {
    public ClienteExistenteException(Integer dni){
        super("Ya existe un cliente cuyo DNI es: " + dni);
    }
}
