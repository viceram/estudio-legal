package estudiolegal.excepcion;

public class ClienteInexistenteException extends Exception{
    public ClienteInexistenteException(Integer dni){
        super("No se ha encontrado un cliente con DNI: " + dni);
    }
}
