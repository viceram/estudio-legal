package estudiolegal;

import java.util.ArrayList;
import java.util.Collections;

import estudiolegal.excepcion.*;

public class EstudioLegal {

    private ArrayList<Cliente> clientes;
    private String telefono;
    private String cuil;

    public EstudioLegal(String cuil, String telefono) {
        this.cuil = cuil;
        this.telefono = telefono;
        this.clientes = new ArrayList<Cliente>();
    }

    //--------------getters------------------
    public ArrayList<Cliente> getClientes() {
        return clientes;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getCuil() {
        return cuil;
    }

    //--------------setters-------------------
    public void agregarClientes(Cliente cliente) throws ClienteExistenteException, DomicilioNoAgregadoException {
        for(Cliente var: clientes){
            if(var.getDni().equals(cliente.getDni())){
                throw new ClienteExistenteException(cliente.getDni());
            }
        }

        if(cliente.getDomicilio() == null){
            throw new DomicilioNoAgregadoException();
        }
        this.clientes.add(cliente);
    }

    public void setCuil(String cuil) {
        this.cuil = cuil;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Cliente getCliente(Integer dni) throws ClienteInexistenteException{
        Cliente clienteEncontrado = null;
        for (Cliente var : clientes) {
            if(var.getDni().equals(dni)){
                clienteEncontrado=var;
                break;
            }
        }

        if(clienteEncontrado == null){
            throw new ClienteInexistenteException(dni);
        }
        return clienteEncontrado;
    }

    public void modificarCliente(Cliente clienteModificado) throws ClienteExistenteException,
                    ClienteInexistenteException, DomicilioNoAgregadoException {
        Cliente clienteEncontrado;
        clienteEncontrado = getCliente(clienteModificado.getDni());
        clientes.remove(clienteEncontrado);
        this.agregarClientes(clienteModificado);
    }

    public void eliminarCliente(Integer dni) throws ClienteInexistenteException{
        Cliente clienteEncontrado;
        clienteEncontrado = getCliente(dni);
        clientes.remove(clienteEncontrado);
    }

    public String mostrarListaDeClientes(){
        String listaClientes = "";
        for(Cliente var: clientes){
            listaClientes = var.mostrarDatosDelCliente() + "\n";
        }
        return listaClientes;
    }
    
    public int cantidadDeClientes(){
        return clientes.size();
    }

    public void ordenarClientesPorDni(){
        Collections.sort(clientes);
    }

    public void ordenarClientesPorApellido(){
        Collections.sort(clientes, new ComparadorPorApellido());
    }
}
